﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using  UnityStandardAssets.ImageEffects;

public class HUDManager : MonoBehaviour
{
	public Button restartGameButton;
	public Button bonusButton, bonusButtonUmbrella;
	public Button BackToMenuBtn, RestartCurrentStageBtn, NextStageBtn;
	public Button BackToMenuBtn_stageLost, RestartCurrentStageBtn_stageLost;
	public GameObject panelStageWon, panelStageStart, panelStageLost;
	public static HUDManager instance;
	public Text textPoints, textStageStart, textStageWon;
	//public Text tHpPoints;
	float fHpPoints;
	public Image imgHpPoints, imgBonusHero, imgBonusUmbrella, imgProgressBar;
	public GameObject Backgrounds;
	public Creature oPlayerCreature;
	PlayerData dataManagerPlayerData;
	float HeroSpeedHor;
	public Camera mainCamera, cameraBackground;
	BlurOptimized mainCameraBlur,cameraBackgroundBlur;
	public GameObject MainCanvas;

	private void Awake()
	{
		if (instance == null) {
			//DontDestroyOnLoad (transform.root.gameObject);
			instance = this;
		} else if (instance != this) {
			//Destroy (gameObject);
			return;
		}
	}

	void Start ()
	{	
		

		dataManagerPlayerData = DataManager.instance.oPlayerData;
		restartGameButton.onClick.AddListener(() => DataManager.instance.GoToMainMenu());

		BackToMenuBtn.onClick.AddListener(() => DataManager.instance.GoToMainMenu());
		NextStageBtn.onClick.AddListener(() => DataManager.instance.StartNextStageFromGameplay());
		RestartCurrentStageBtn.onClick.AddListener(() => DataManager.instance.RestartStageFromGameplay());

		BackToMenuBtn_stageLost.onClick.AddListener(() => DataManager.instance.GoToMainMenu());
		RestartCurrentStageBtn_stageLost.onClick.AddListener(() => DataManager.instance.StartGameFromMenuOrRestart(DataManager.instance.currentStage));

		bonusButton.onClick.AddListener(() => BonusManager.instance.HeroBonusStart());
		bonusButtonUmbrella.onClick.AddListener(() => PlayerSingleton.instance.GetComponentInChildren<HeroShootingController>().HeroShooting());
		InitHudForPlayer ();
	}

	void OnDisable(){
		GlobalEventsManager.instance.onPointsChange -= this.updatePoints;
		oPlayerCreature.OnPlayerHpChange -= this.updateHP;
		//GlobalEventsManager.instance.onGameStateChange -= this.StageEnd;
	}

	public void InitHudForPlayer(){
		Debug.Log ("INIT HUD");
		mainCameraBlur = mainCamera.GetComponent<BlurOptimized> ();
		cameraBackgroundBlur = cameraBackground.GetComponent<BlurOptimized> ();
		oPlayerCreature.OnPlayerHpChange += this.updateHP;
		//GlobalEventsManager.instance.onGameStateChange += this.StageEnd;
		//oPlayerCreature.OnPlayerDeath += this.StageEnd;
		oPlayerCreature.Health = oPlayerCreature.Health;
		oPlayerCreature.HorizontalSpeed = oPlayerCreature.MaxSpeed;
		GlobalEventsManager.instance.onPointsChange += this.updatePoints;
		textPoints.text ="POINTS: "+( dataManagerPlayerData.Points).ToString();

		imgProgressBar.fillAmount = (dataManagerPlayerData.HighScore / 1000f);
		GlobalEventsManager.instance.TriggerNpcActivate (dataManagerPlayerData.HighScore);
	}

	public void setBlur(bool enableBlur){
		mainCameraBlur.enabled = enableBlur;
		cameraBackgroundBlur.enabled = enableBlur;
		MainCanvas.SetActive (false);
	}
		
	void updatePoints(int points, int killPoints){
		dataManagerPlayerData.KillPoints = dataManagerPlayerData.KillPoints + killPoints;
		dataManagerPlayerData.Points = dataManagerPlayerData.Points+points;
		if (dataManagerPlayerData.Points<=0) {
			dataManagerPlayerData.Points = 0;
		}
		dataManagerPlayerData.CoinsCount = dataManagerPlayerData.CoinsCount+points;
		if (dataManagerPlayerData.KillPoints>=DataManager.instance.currentStage.Number*DataManager.instance.killsNeedToWinMultipler) {
			DataManager.instance.maxEnemiesCount = 0;
			StartCoroutine(waitingForLastKill());
		}

		if (dataManagerPlayerData.HighScore < dataManagerPlayerData.Points) {
			dataManagerPlayerData.HighScore = (int)dataManagerPlayerData.Points;
			GlobalEventsManager.instance.TriggerNpcActivate (dataManagerPlayerData.HighScore);
			}
		textPoints.text ="POINTS: "+( dataManagerPlayerData.Points).ToString();
		imgProgressBar.fillAmount = (dataManagerPlayerData.HighScore / 1000f);
	}

	IEnumerator waitingForLastKill(){
		yield return new WaitForSeconds (2);
		if (DataManager.instance.currentEnemiesCount==0) {
			yield return new WaitForSeconds (1);
			GlobalEventsManager.instance.triggerGameStateChange (GameState.StageWon);
		}
	}
	//TODO!!!
	public void updateHP(float newHp){
			imgHpPoints.fillAmount = (newHp / 10f);
	}

	void showBackgroundImage(float bgNumber){
		if (Backgrounds.transform.Find("ImageBackground"+bgNumber)!=null && Backgrounds.transform.Find("ImageBackground"+bgNumber).gameObject.activeSelf==false ) {
		Backgrounds.transform.Find("ImageBackground"+bgNumber).gameObject.SetActive(true);
		}
	}

	public void RestartGame_BackToMenu ()
	{
		DataManager.instance.GoToMainMenu ();
	}

	IEnumerator restartGameDelay(float delayTime){
		yield return new WaitForSeconds (delayTime);
	}

}

