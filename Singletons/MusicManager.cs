﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{

	Object[] myMusic; // declare this as Object array
	AudioSource audioSrc;

	public static MusicManager instance;
	void Awake () {
		myMusic = Resources.LoadAll("Music",typeof(AudioClip));
		audioSrc = GetComponent<AudioSource> ();
		audioSrc.clip = myMusic[Random.Range(0,myMusic.Length)] as AudioClip;
		if (!audioSrc.isPlaying) {
			playRandomMusic ();
		}

		if (instance == null)
			instance = this;
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(instance.gameObject);


	}


	void Start (){
		audioSrc.Play(); 
	}

	// Update is called once per frame
	void Update () {

		if (!audioSrc.isPlaying && Time.time>20) {
			audioSrc.clip = myMusic[Random.Range(0,myMusic.Length)] as AudioClip;
			playRandomMusic ();
		} 
		if (!audioSrc.isPlaying && Time.time<=20) {
			//audioSrc.clip = myMusic[Random.Range(0,myMusic.Length)] as AudioClip;
			playRandomMusic ();
		} 
//		else {
//			audioSrc.clip = audioSrc.clip;
//			playRandomMusic ();
//		}
	}

	void playRandomMusic() {
		
		audioSrc.Play();
	}
}