﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

	public static string POOLED_PREFAB_PATH = "Prefabs/";
	public static PoolManager instance;
	public GameObject BulletsPool, EnemiesPool,ExplosionsPool, PowerupsPool, FlowersPool;
	public int pooledAmount = 10;
	public bool WillGrow = true;
	List<GameObject> pooledBullets, pooledEnemies, pooledExplosions, pooledPowerups, pooledFlowers;


	void Awake()
	{  
		instance = this;
		pooledExplosions = new List<GameObject>();
		pooledBullets=new List<GameObject>();
		pooledEnemies=new List<GameObject>();
		pooledPowerups=new List<GameObject>();
		pooledFlowers=new List<GameObject>();
		//
//		createExplosions(4, ExplosionType.Bird);
		createExplosions(4, ExplosionType.LaserBulletExplosion);
		createExplosions(3, ExplosionType.EnemyUfoExplosion);
		createExplosions(13, ExplosionType.Fireball);

	//	createBullets(15, BulletType.EggBullet);
		createBullets(15, BulletType.UfoBullet);
		createBullets(3, BulletType.DragonFire);
		createBullets(3, BulletType.ElectroBullet);
	

		//todo
		createEnemies(2, DataManager.instance.currentStage.Number-1);
		createEnemies(2, DataManager.instance.currentStage.Number);
		createEnemies(2, DataManager.instance.currentStage.Number-2);


		createPowerups (1, PowerupType.Powerup1);
		createPowerups (1, PowerupType.Powerup2);
		createPowerups (1, PowerupType.Powerup3);

		createFlowers (3, "FlowerGround1");
		createFlowers (3, "FlowerGround2");
		createFlowers (3, "FlowerGround3");
	}

	void createExplosions(int count, ExplosionType explosionType)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Explosions/"+explosionType.ToString()));

			obj.SetActive(false);
			obj.name = explosionType.ToString();
			pooledExplosions.Add(obj);
			obj.transform.SetParent(ExplosionsPool.transform);
		}

	}


	void createBullets(int count, BulletType bulletType)
	{
		
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Bullets/"+ bulletType.ToString()));
			obj.SetActive(false);
			obj.name = bulletType.ToString();
			pooledBullets.Add(obj);
			obj.transform.SetParent(BulletsPool.transform);
		}

	}


	void createEnemies(int count, int enemyNumber)
	{
		for (int i = 0; i < count; i++)
		{
			//Debug.Log (enemyType);
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Enemies/enemy"+enemyNumber.ToString()));
			obj.SetActive(false);
			obj.name = "enemy"+enemyNumber.ToString();
			pooledEnemies.Add(obj);
			obj.transform.SetParent(EnemiesPool.transform);
		}

	}

	void createPowerups(int count, PowerupType powerupType)
	{
		for (int i = 0; i < count; i++)
		{
			//Debug.Log (enemyType);
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Powerups/"+powerupType.ToString()));
			obj.SetActive(false);
			obj.name = powerupType.ToString();
			pooledPowerups.Add(obj);
			obj.transform.SetParent(PowerupsPool.transform);
		}

	}

	void createFlowers(int count, string flowerName)
	{
		for (int i = 0; i < count; i++)
		{
//			Debug.Log (flowerName);
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Flowers/"+flowerName));

			obj.SetActive(false);
			obj.name = flowerName;
			pooledFlowers.Add(obj);
			obj.transform.SetParent(FlowersPool.transform);
		}

	}

//
	public GameObject GetPooledObject_Explosion(ExplosionType explosionType)
	{
		foreach (GameObject item in pooledExplosions)
		{
			if (item.name == explosionType.ToString())
			{     
				if (!item.activeInHierarchy)
				{

					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Explosions/"+ explosionType.ToString()));
			obj.name=explosionType.ToString();
			pooledExplosions.Add(obj);
			obj.transform.SetParent(ExplosionsPool.transform);

			return obj;


		}
		return null;
	}



	public GameObject GetPooledObject_Bullet(BulletType bulletType)
	{
		foreach (GameObject item in pooledBullets)
		{
			if (item.name == bulletType.ToString())
			{     
				if (!item.activeInHierarchy)
				{

					return item;
				}

			}
		}
		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH + "Bullets/" +bulletType.ToString()));
			obj.name=bulletType.ToString();
			pooledBullets.Add(obj);
			obj.transform.SetParent(BulletsPool.transform);

			return obj;

		}
		return null;

	}


	public GameObject GetPooledObject_Enemy(int enemyNumber)
	{
		foreach (GameObject item in pooledEnemies)
		{
			if (item.name == enemyNumber.ToString())
			{     
				if (!item.activeInHierarchy)
				{

					return item;
				}

			}
		}
		//        WillGrow=false;
		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate (Resources.Load (POOLED_PREFAB_PATH + "Enemies/enemy" + enemyNumber.ToString ()));
			obj.name = enemyNumber.ToString ();
				pooledEnemies.Add (obj);
				obj.transform.SetParent (EnemiesPool.transform);
				return obj;
			}

		return null;

	}
		


	public GameObject GetPooledObject_Powerup(PowerupType powerupType)
	{
		foreach (GameObject item in pooledPowerups)
		{
			if (item.name == powerupType.ToString())
			{     
				if (!item.activeInHierarchy)
				{

					return item;
				}

			}
		}
		//        WillGrow=false;
		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate (Resources.Load (POOLED_PREFAB_PATH + "Powerups/" + powerupType.ToString ()));
			obj.name = powerupType.ToString ();
			pooledPowerups.Add (obj);
			obj.transform.SetParent (PowerupsPool.transform);
			return obj;
		}

		return null;

	}


	public GameObject GetPooledObject_Flower(string flowerName)
	{
		foreach (GameObject item in pooledFlowers)
		{
			if (item.name == flowerName)
			{     
				if (!item.activeInHierarchy)
				{

					return item;
				}

			}
		}
		//        WillGrow=false;
		if (WillGrow)
		{
			Debug.Log (flowerName);
			GameObject obj = (GameObject)Instantiate (Resources.Load (POOLED_PREFAB_PATH + "Flowers/" + flowerName));
			obj.name = flowerName;
			pooledFlowers.Add (obj);
			obj.transform.SetParent (FlowersPool.transform);
			return obj;
		}

		return null;

	}



}




