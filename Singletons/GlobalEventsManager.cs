﻿
using UnityEngine;
using System.Collections;
using System;

public class GlobalEventsManager : MonoBehaviour
{
	public static GlobalEventsManager instance;



	private void Awake()
	{
		//DontDestroyOnLoad(transform.root.gameObject);
		if (instance == null)
			instance = this;
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(instance.gameObject);
	}

	public delegate void NpcActivate(int highScore);
	public event NpcActivate OnNpcActivate;

	public void TriggerNpcActivate(int highScore){
		if (OnNpcActivate!=null) {
			OnNpcActivate (highScore);// wywloanie eventa tak zeby wsyzscy co subksyrbuja do niego wiedzieli o tym
		}
	}


	public delegate void SwitchMode();
	public event SwitchMode onSwitchMode;


	public void triggerSwitchMode(){
		if (onSwitchMode!=null) {
			onSwitchMode ();
		}
	}


	public delegate void GetPoints(int points,int killPoints);
	[field:NonSerialized]
	public event GetPoints onPointsChange;


	public void triggerGetPoints(int points, int killPoints){
		if (onPointsChange!=null) {
			onPointsChange (points,killPoints);
		}
	}

	public delegate void GetCurrentHero(Creature hero);
	public event GetCurrentHero onCurrentHeroChangee;

	public void triggerGetCurrentHero(Creature hero){
		if (onCurrentHeroChangee!=null) {
			onCurrentHeroChangee (hero);
		}
	}

	public delegate void GetCurrentUmbrella(Umbrella umbrella);
	public event GetCurrentUmbrella onCurrentUmbrellaChange;

	public void triggerGetCurrentUmbrella(Umbrella umbrella){
		if (onCurrentUmbrellaChange!=null) {
	
			onCurrentUmbrellaChange (umbrella);
		}
	}


	public delegate void GameStateChange(GameState newGameState);
	[field:NonSerialized]
	public event GameStateChange onGameStateChange;

	public void triggerGameStateChange(GameState newGameState){
		if (onGameStateChange!=null) {
			onGameStateChange (newGameState);
		}
	}


//	public delegate void UIumbrellaChange(string newUmbrellaUI);
//	public event UIumbrellaChange OnUmbrellaUIChange;
//
//	public void TriggerPlayerHpChange(string newUmbrellaUI){
//		if (OnUmbrellaUIChange!=null) {
//			OnUmbrellaUIChange (newUmbrellaUI);// wywloanie eventa tak zeby wsyzscy co subksyrbuja do niego wiedzieli o tym
//		}
//	}

}