﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
	public GameObject displayHero, displayUmbrella;
	public Button startGameButton1,startGameButton2,startGameButton3;

	public Text txtCoins;
	public static UIManager instance;

	public delegate void updateUI();
	public event updateUI onUIchange;


	public void triggerUpdateUI(){
		if (onUIchange!=null) {
			onUIchange ();
		}
	}


	private void Awake()
	{
		
		if (instance == null) {
		//	DontDestroyOnLoad (transform.root.gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
			return;
		}
	}
		
	void Start(){
		UIManager.instance.onUIchange += this.UIupdate;
		txtCoins.text="COINS: "+ DataManager.instance.oPlayerData.CoinsCount;
		UIManager.instance.triggerUpdateUI ();
	}

	void OnEnable(){

	}


	public void InitUImanagerForPlayer(){
	}

	void OnDisable(){
		UIManager.instance.onUIchange -= this.UIupdate;
	}



	public void UIupdate(){

		txtCoins.text="COINS: "+ DataManager.instance.oPlayerData.CoinsCount;
		if (DataManager.instance.oPlayerData.HeroesList.Count > 0) {
			DataManager.instance.updateHeroesList ();
		}else	{
			DataManager.instance.makeHeroesList ();
		}

		if (DataManager.instance.oPlayerData.HeroesList.Count > 0) {
			DataManager.instance.updateUmbrellaList ();
		}else	{
			DataManager.instance.makeUmbrellasList ();
		}

		txtCoins.text="COINS: "+ DataManager.instance.oPlayerData.CoinsCount;

	}
}

