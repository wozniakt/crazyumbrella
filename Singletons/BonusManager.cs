﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BonusManager : MonoBehaviour
{
	public int bonusCounter,reflectCounter; 
	public int pointsActivateBonus, reflectCounterActivateBonus;

	//public Button bonusButton;// not need to be public!
	// Use this for initialization

	Vector2 heroStartPos;
	public static BonusManager instance;
	private void Awake()
	{

		if (instance == null) {
			//	DontDestroyOnLoad (transform.root.gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
			return;
		}
	}
	void Start ()
	{
		HUDManager.instance.bonusButton.interactable = false;
		GlobalEventsManager.instance.onPointsChange += this.GiveBonusToPlayer;
		heroStartPos = GameObject.Find ("heroStartPos").transform.localPosition;
	}
	void OnDisable(){
		GlobalEventsManager.instance.onPointsChange -= this.GiveBonusToPlayer;

	}

	void GiveBonusToPlayer (int points,int killPoints)
	{
		bonusCounter += killPoints;
		HUDManager.instance.imgBonusHero.fillAmount = ((float)bonusCounter % pointsActivateBonus) / pointsActivateBonus;
		if (bonusCounter>=pointsActivateBonus) {
			HUDManager.instance.imgBonusHero.fillAmount = 1;
			HUDManager.instance.bonusButton.interactable = true;
		}

	}

	public void GiveBonusToUmbrella ()
	{
		
		HUDManager.instance.imgBonusUmbrella.fillAmount = ((float)reflectCounter % reflectCounterActivateBonus) / reflectCounterActivateBonus;

		if (reflectCounter>=reflectCounterActivateBonus) {
			HUDManager.instance.imgBonusUmbrella.fillAmount = 1;
			HUDManager.instance.bonusButtonUmbrella.interactable = true;
		}
	}


	public void HeroBonusStart(){
		GameObject bonus=(GameObject)Instantiate(Resources.Load("Prefabs/Bonuses/" + "Bonus"+DataManager.instance.currentHero.Name));
		bonus.transform.localPosition = heroStartPos;
		HUDManager.instance.bonusButton.interactable = false;
		bonusCounter = 0;
		HUDManager.instance.imgBonusHero.fillAmount = ((float)bonusCounter % pointsActivateBonus)/pointsActivateBonus;
	}

	public void bonusNPCstart(){
		
	}

}

