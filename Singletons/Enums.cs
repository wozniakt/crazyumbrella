﻿using UnityEngine;
using System.Collections;


public enum BulletType {UfoBullet, DragonFire, Empty, TankBullet, ElectroBullet, SilverFire, PlasmaBullet, RainDrop, FlowerBullet}

public enum ExplosionType {Fireball, EnemyUfoExplosion, LaserBulletExplosion, TankBulletExplosion,
GreenMonsterExplosion, Splash, FlowerBulletExplosion}

public enum PowerupType {Powerup1, Powerup2, Powerup3} 
public enum FlowerType {Flower1, Flower2, Flower3} 

//public enum EnemyType {enemy_x,enemy_y,enemy1,enemy2,enemy3,enemy4,enemy5,enemy6,enemy7, enemy8, enemy9, enemy10,
//	enemy11,enemy12,enemy13,enemy14,enemy15,enemy16,enemy17,enemy18, enemy19, enemy20, enemy21,
//	enemy22,enemy23,enemy24,enemy25,enemy26,enemy27,enemy28, enemy29, enemy30}

public enum AccessStatus { isLocked,isUnlocked}

public enum UmbrellaMode { shield,shooting}


public enum GameState {GameIsOn, StageWon, StageLost, MainMenu, Pause}