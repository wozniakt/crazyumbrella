﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{

	public  Creature currentHero;
	public Umbrella currentUmbrella;
	public int currentEnemiesCount, maxEnemiesCount;
	GameObject hero, chest;
	public static DataManager instance;
	public PlayerData oPlayerData;
	public UmbrellaMode currentUmbMode;
	public bool withSlowingMode;
	public Stage currentStage;
	public float killsNeedToWinMultipler;
	//	public int maxUnlockedStage;
	public GameState oGameState;
	 GameObject blurCamera;
	//public int lastUsedHero, lastUsedUmbrella;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
		loadData ();

	}

	void Start ()
	{
		currentUmbMode = UmbrellaMode.shield;
		makeHeroesList ();
		makeUmbrellasList ();


		GlobalEventsManager.instance.onCurrentHeroChangee += this.heroChange;
		GlobalEventsManager.instance.onCurrentUmbrellaChange += this.umbrellaChange;
		GlobalEventsManager.instance.onGameStateChange += this.onChangeStateGame;
		UIManager.instance.onUIchange += updateHeroesList;
		UIManager.instance.onUIchange += updateUmbrellaList;
		UIManager.instance.triggerUpdateUI ();
	}

	void onChangeStateGame (GameState gameState)
	{
		DataManager.instance.oGameState = gameState;
		switch (gameState) {
		case GameState.GameIsOn:
			//HUDManager.instance.setBlur (false);
			Time.timeScale = 1;
			HUDManager.instance.panelStageStart.SetActive (true);
			HUDManager.instance.textStageStart.text = "STAGE" + DataManager.instance.currentStage.Number;
			//HUDManager.instance.setBlur (false);
			break;
		case GameState.MainMenu:
			HUDManager.instance.setBlur (false);
			Time.timeScale = 1;
			break;
		case GameState.Pause:
			HUDManager.instance.setBlur (true);
			break;
		case GameState.StageLost:
			HUDManager.instance.panelStageLost.SetActive (true);
			HUDManager.instance.setBlur (true);
			Time.timeScale = 0;
			DataManager.instance.oPlayerData.KillPoints = 0;
			break;
		case GameState.StageWon:
			HUDManager.instance.setBlur (true);
			HUDManager.instance.panelStageWon.SetActive (true);
			Time.timeScale = 0;
			if (oPlayerData.MaxUnlockedStage <= currentStage.Number) {
				oPlayerData.MaxUnlockedStage = currentStage.Number + 1;
			}
			HUDManager.instance.textStageWon.text = "STAGE CLEAR";
			DataManager.instance.oPlayerData.KillPoints = 0;
			break;
		}
	}

	public void StartNextStageFromGameplay ()
	{
		HUDManager.instance.panelStageWon.SetActive (false);
		currentStage.Number = currentStage.Number + 1;
		DataManager.instance.StartGameAfterWin (currentStage);
	}

	public void RestartStageFromGameplay ()
	{
		HUDManager.instance.panelStageWon.SetActive (false);
		HUDManager.instance.panelStageLost.SetActive (false);
		currentStage.Number = currentStage.Number;

		DataManager.instance.StartGameFromMenuOrRestart (currentStage);

	}

	void heroChange (Creature hero)
	{
		currentHero = hero;
	}

	void umbrellaChange (Umbrella umbrella)
	{
		currentUmbrella = umbrella;

	}


	public void StartGameFromMenuOrRestart (Stage oStage)
	{
		if (currentHero.IsUnlocked && currentUmbrella.IsUnlocked && oStage.IsUnlocked) {
			currentStage = oStage;
			//saveData ();
			oPlayerData = SaveLoadData.LoadedPlayerData;
			currentHero.Health = currentHero.MaxHealth;
			oPlayerData.Points = 0;
			StartCoroutine (startGameDelay (0.2f));
		} else {
			Debug.Log ("NOT ENOUGH MONEY EVENT");
		}
	}

	public void StartGameAfterWin (Stage oStage)
	{
		if (currentHero.IsUnlocked && currentUmbrella.IsUnlocked && oStage.IsUnlocked) {
			currentStage = oStage;
			//saveData ();
			//oPlayerData = SaveLoadData.LoadedPlayerData;
			//oPlayerData.Points = 0;
			StartCoroutine (startGameDelay (0.2f));
		} else {
			Debug.Log ("NOT ENOUGH MONEY EVENT");
		}
	}



	IEnumerator startGameDelay (float delayTime)
	{//	DataManager.instance.oGameState = GameState.GameIsOn;
		AsyncOperation async = SceneManager.LoadSceneAsync ("UmbrellaGameplay");

		yield return async;
		//DataManager.instance.oGameState = GameState.GameIsOn;  // to czy to poniżej?
		//GlobalEventsManager.instance.triggerGameStateChange (GameState.GameIsOn);
		Debug.Log ("LOADED LEVEL"+DataManager.instance.oGameState);
		hero = (GameObject)Instantiate (Resources.Load ("Prefabs/Heroes/" + currentHero.Name));
		hero.GetComponent<CreatureMonoModel> ().oCreature = currentHero;
		hero.transform.parent = GameObject.Find ("Gameplay").transform;
		hero.transform.position = GameObject.Find ("heroStartPos").transform.localPosition;
		hero.SetActive (true);
		GameObject umbrella = (GameObject)Instantiate (Resources.Load ("Prefabs/Heroes/" + currentUmbrella.Name));
		umbrella.transform.parent = hero.transform;
		umbrella.transform.position = GameObject.Find ("heroStartPos").transform.localPosition;
		umbrella.SetActive (true);
		GameObject ground = (GameObject)Instantiate (Resources.Load ("Prefabs/Grounds/Ground" + currentStage.Number));
		ground.transform.parent = GameObject.Find ("GroundPlaceHolder").transform;
		ground.name = ground.name.Replace ("(Clone)", "");
		ground.transform.position = GameObject.Find ("GroundPlaceHolder").transform.localPosition;
		ground.SetActive (true);
		GameObject chest = (GameObject)Instantiate (Resources.Load ("Prefabs/Heroes/" + "Chest"));
		chest.transform.parent =  GameObject.Find ("Gameplay").transform;
		chest.transform.position =new Vector2( GameObject.Find ("heroStartPos").transform.localPosition.x+Random.Range(-2,2),
			GameObject.Find ("heroStartPos").transform.localPosition.y);
		chest.SetActive (true);

		HUDManager.instance.oPlayerCreature = hero.GetComponent<CreatureMonoModel> ().oCreature;
	
		GlobalEventsManager.instance.triggerGameStateChange (GameState.GameIsOn);

	}

	public void GoToMainMenu ()
	{
		StartCoroutine (startMenuDelay (0.2f));

	}

	IEnumerator startMenuDelay (float delayTime)
	{
		GlobalEventsManager.instance.triggerGameStateChange (GameState.MainMenu);
		AsyncOperation async = SceneManager.LoadSceneAsync ("Menu");
		oPlayerData.Points = 0;

		yield return async;

		yield return new WaitForSeconds (0.001f);// toDO trzeba prawdopodobnie zamienić na innego yielda - jakiś Waituntil albo cośS
		UIManager.instance.triggerUpdateUI ();

		//saveData ();
	}


	public void saveData ()
	{
		SaveLoadData.Save (oPlayerData);
	}

	public void loadData ()
	{
		SaveLoadData.Load ();

		oPlayerData = SaveLoadData.LoadedPlayerData;
		UIManager.instance.triggerUpdateUI ();
	}

	public void makeHeroesList ()
	{
		oPlayerData.HeroesList = new List<Creature> ();
		foreach (CreatureMonoModel oCreature in GameObject.FindObjectsOfType<CreatureMonoModel>()) {
			oPlayerData.HeroesList.Add (oCreature.GetComponent<CreatureMonoModel> ().oCreature);
		}
	}

	public void makeUmbrellasList ()
	{
		oPlayerData.UmbrellaList = new List<Umbrella> ();
		foreach (UmbrellaMonoModel oUmbrella in GameObject.FindObjectsOfType<UmbrellaMonoModel>()) {
			oPlayerData.UmbrellaList.Add (oUmbrella.GetComponent<UmbrellaMonoModel> ().oUmbrella);
			//Debug.Log(oPlayerData.UmbrellaList.Count);
		}
	}

	public void updateHeroesList ()
	{
		//saveData ();
		foreach (Creature oLoadedHero in oPlayerData.HeroesList) {
			foreach (CreatureMonoModel oCreature in GameObject.FindObjectsOfType<CreatureMonoModel>()) {
				if (oLoadedHero.Name == oCreature.GetComponent<CreatureMonoModel> ().oCreature.Name) {
					oCreature.GetComponent<CreatureMonoModel> ().oCreature = oLoadedHero;
					updateStatus_BoughtUnlocked (oCreature.GetComponent<Button> (), oCreature.oCreature.IsUnlocked);
				}
			}
		}
	}


	public void updateUmbrellaList ()
	{
		//saveData ();
		foreach (Umbrella oLoadedUmbrella in oPlayerData.UmbrellaList) {
			foreach (UmbrellaMonoModel oUmbrella in GameObject.FindObjectsOfType<UmbrellaMonoModel>()) {
				if (oLoadedUmbrella.Name == oUmbrella.GetComponent<UmbrellaMonoModel> ().oUmbrella.Name) {
					oUmbrella.GetComponent<UmbrellaMonoModel> ().oUmbrella = oLoadedUmbrella;
					updateStatus_BoughtUnlocked (oUmbrella.GetComponent<Button> (), oUmbrella.oUmbrella.IsUnlocked);
//					Debug.Log (oUmbrella.oUmbrella.Name + " " + oUmbrella.oUmbrella.IsUnlocked);
				}
			}
		}
	}


	public void ClearPLayerData ()
	{
		SaveLoadData.ClearData ();
		oPlayerData = new PlayerData ();
		saveData ();
	}

	//TODO przenieść do UIMANAGERA, zapytać mateo jak, albo się zastanowić
	void updateStatus_BoughtUnlocked (Button button, bool isVisible)
	{
		Color buttonColor = button.GetComponent<Image> ().color;
		if (!isVisible) {
			buttonColor.r = 0;
			buttonColor.g = 0;
			buttonColor.b = 0;
		}
		if (isVisible) {
			buttonColor.r = 1;
			buttonColor.g = 1;
			buttonColor.b = 1;
		}
		button.GetComponent<Image> ().color = buttonColor;
	}

	public void addCoins_developMode ()
	{
		oPlayerData.CoinsCount += 100;
		UIManager.instance.triggerUpdateUI ();
	}

	void OnDisable ()
	{
		GlobalEventsManager.instance.onCurrentHeroChangee -= this.heroChange;
		GlobalEventsManager.instance.onCurrentUmbrellaChange -= this.umbrellaChange;
		GlobalEventsManager.instance.onGameStateChange -= this.onChangeStateGame;
		UIManager.instance.onUIchange -= updateHeroesList;
		UIManager.instance.onUIchange -= updateUmbrellaList;

	}



}

