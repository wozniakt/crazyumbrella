﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
	public class Umbrella
	{

	[SerializeField]
	bool lastUsed;
	public bool LastUsed
	{
		get
		{return lastUsed;}
		set{

			lastUsed= value ;

			//}
		}

	}


	[SerializeField]
	int id;
	public int Id
	{
		get
		{return id;}
		set{

			id= value;

		}
	}

			[SerializeField]
			string name;
			public string Name
			{
				get
				{return name;}
				set{

					name= value;

				}
			}

	[SerializeField]
	int cost;
	public int Cost
	{
		get
		{return cost;}
		set{

			cost= value;

		}
	}


	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked
	{
		get
		{return isUnlocked;}
		set{

			isUnlocked= value;

		}
	}
		}



