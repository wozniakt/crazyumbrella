﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Stage
{



	[SerializeField]
	int number;
	public int Number
	{
		get
		{return number;}
		set{

			number= value;

		}
	}

	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked
	{
		get
		{return isUnlocked;}
		set{

			isUnlocked= value;

		}
	}
}