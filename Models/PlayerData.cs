﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class PlayerData 
{
	[SerializeField]
	int maxUnlockedStage;
	public int MaxUnlockedStage
	{
		get
		{return maxUnlockedStage;}
		set{

			maxUnlockedStage= value;
			//coinsCount = DataManager.instance.coins;
		}
	}


	[SerializeField]
	int lastUsedHero;
	public int LastUsedHero
	{
		get
		{return lastUsedHero;}
		set{

			lastUsedHero= value;
			//coinsCount = DataManager.instance.coins;
		}
	}

	[SerializeField]
	int lastUsedUmb;
	public int LastUsedUmb
	{
		get
		{return lastUsedUmb;}
		set{

			lastUsedUmb= value;
			//coinsCount = DataManager.instance.coins;
		}
	}




	[SerializeField]
	int coinsCount;
	public int CoinsCount
	{
		get
		{return coinsCount;}
		set{

			coinsCount= value;
			//coinsCount = DataManager.instance.coins;
		}
	}

	[SerializeField]
	List<Umbrella> umbrellaList;
	public List<Umbrella> UmbrellaList{
		get
		{return umbrellaList;}
		set{

			umbrellaList = value;  //new List<Creature> (DataManager.instance.heroesList);
			//		coinsCount = DataManager.instance.coins;
		}
	}



	[SerializeField]
	List<Creature> heroesList;
	public List<Creature> HeroesList{
		get
		{return heroesList;}
		set{

			heroesList = value;  //new List<Creature> (DataManager.instance.heroesList);
			//		coinsCount = DataManager.instance.coins;
		}
	}

	[SerializeField]
	int highScore;
	public int HighScore
	{
		get
		{return highScore;}
		set{

			highScore=value;
			//		coinsCount = DataManager.instance.coins;
		}
	}

	[SerializeField]
	float points;
	public float Points
	{
		get
		{return points;}
		set{

			points=value;
			//		coinsCount = DataManager.instance.coins;
		}
	}

	[SerializeField]
	float killPoints;
	public float KillPoints
	{
		get
		{return killPoints;}
		set{

			killPoints=value;
			//		coinsCount = DataManager.instance.coins;
		}
	}

}


