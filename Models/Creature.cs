﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


[System.Serializable]
public class Creature
	{


		[SerializeField]
		string name;
		public string Name
		{
			get
			{return name;}
			set{

				name= value;

			}
		}

	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked
	{
		get
		{return isUnlocked;}
		set{

			isUnlocked= value;

		}
	}

	[SerializeField]
	int cost;
	public int Cost
	{
		get
		{return cost;}
		set{

			cost= value;

		}
	}

	[SerializeField]
	int id;
	public int Id
	{
		get
		{return id;}
		set{

			id= value;

		}
	}



	[SerializeField]
	float maxSpeed;
	public float MaxSpeed
	{
		get
		{return maxSpeed;}
		set{

			maxSpeed= value;

		}
	}

	[SerializeField]
	float horizontalSpeed;
	public float HorizontalSpeed
	{
		get
		{return horizontalSpeed;}
		set{
			//if (horizontalSpeed>=0) {
				horizontalSpeed= value;
			//} 
//			if (horizontalSpeed<0) {
//				horizontalSpeed= 0;
//			} 


		}
	}

	[SerializeField]
	float verticalSpeed;
	public float VerticalSpeed
	{
		get
		{return verticalSpeed;}
		set{

			verticalSpeed= value;

		}
	}

	[SerializeField]
	int maxHealth;
	public int MaxHealth
	{
		get
		{return maxHealth;}
		set{
			maxHealth= value ;
		}
	}


	public delegate void SingleFloatChange(float newHp);
	[field:NonSerialized]
	public event SingleFloatChange OnPlayerHpChange;

	public void TriggerPlayerHpChange(float newHp){
		if (OnPlayerHpChange!=null) {
			OnPlayerHpChange (newHp);// wywloanie eventa tak zeby wsyzscy co subksyrbuja do niego wiedzieli o tym
		}
	}

	public delegate void PLayerDeath(GameState gameState);
	public event PLayerDeath OnPlayerDeath;

	public void TriggerPlayerDeath(GameState gameState){
		if (OnPlayerDeath!=null) {
			OnPlayerDeath (gameState);// wywloanie eventa tak zeby wsyzscy co subksyrbuja do niego wiedzieli o tym
		}
	}




	[SerializeField]
	int health;
	public int Health
	{
		get
		{return health;}
		set{
			health= value ;
			if (value>=0) {
					TriggerPlayerHpChange(value); 
			}
			if (value<=0) {
				TriggerPlayerDeath(GameState.StageLost); 
			}
		}
	}


	[SerializeField]
	ExplosionType explosionType;
	public ExplosionType ExplosionType
	{
		get
		{return explosionType;}
		set{

			explosionType= value ;

				//}
			}

		}

	[SerializeField]
	bool lastUsed;
	public bool LastUsed
	{
		get
		{return lastUsed;}
		set{

			lastUsed= value ;

			//}
		}

	}

	public void TakeDamage(int damage){
		this.Health -= damage;
	}

}


