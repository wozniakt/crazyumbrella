﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


[System.Serializable]
public class NpcPlaceHolder
{





	[SerializeField]
	string name;
	public string Name
	{
		get
		{return name;}
		set{

			name= value;

		}
	}

	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked
	{
		get
		{return isUnlocked;}
		set{

			isUnlocked= value;

		}
	}

	[SerializeField]
	int highscorePointsToUnlock;
	public int HighscorePointsToUnlock
	{
		get
		{return highscorePointsToUnlock;}
		set{

			highscorePointsToUnlock= value;

		}
	}

	[SerializeField]
	int id;
	public int Id
	{
		get
		{return id;}
		set{

			id= value;

		}
	}
		


	[SerializeField]
	ExplosionType explosionType;
	public ExplosionType ExplosionType
	{
		get
		{return explosionType;}
		set{

			explosionType= value ;

			//}
		}

	}



}


