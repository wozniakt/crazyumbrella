﻿using UnityEngine;
using System.Collections;

public class MoveTowards : MonoBehaviour {
//	public GameObject Target;
	Rigidbody2D rigidbody2d;
//	public float Speed;


	void Start () {
//
		rigidbody2d = GetComponent<Rigidbody2D> ();
//		Vector2 direction = Target.transform.position-transform.position;
//		direction.Normalize();
//		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
//		transform.rotation = Quaternion.Euler(0f, 0f, angle - 45);
		//		this.rigidbody2d.velocity = direction * 5;
	}
		void Update(){
		Vector2 moveDirection = rigidbody2d.velocity;
			if (moveDirection != Vector2.zero) {
				float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
				transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			}

	}
		
	}
