﻿using UnityEngine;
using System.Collections;

public class DetachFromParent : MonoBehaviour
{
	public float timeToDetach;
	void OnEnable ()
	{
		StartCoroutine (detach(timeToDetach));

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("Enemy")) {
			
		//	if (transform.parent!=null) {
				transform.parent = transform.Find("ExplosionsPool");
				this.gameObject.SetActive (false);


			//}
		}
	}
	// Update is called once per frame
	IEnumerator detach (float timeToDetach)
	{
		yield return new WaitForSeconds (timeToDetach);
	//	if (transform.parent!=null) {
			
			transform.parent = transform.Find ("ExplosionsPool");
			this.gameObject.SetActive (false);


		//}
	}

}

