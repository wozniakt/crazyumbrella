﻿using UnityEngine;
using System.Collections;

public class DisableAfterTime : MonoBehaviour
{
	public float timeToDisable;
	// Use this for initialization
	void OnEnable ()
	{
		StartCoroutine (disable ());

	}
	
	// Update is called once per frame
	IEnumerator disable ()
	{
		yield return new WaitForSeconds (timeToDisable);
		this.gameObject.SetActive (false);
	//	Destroy (this.gameObject);
	}
}

