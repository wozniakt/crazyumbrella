﻿using UnityEngine;
using System.Collections;

public class UmbFireball : MonoBehaviour
{
	GameObject fireball;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnCollisionEnter2D (Collision2D col){
		if ( col.transform.CompareTag("Bullet")) {
			col.gameObject.layer = 8;
			fireball=PoolManager.instance.GetPooledObject_Explosion(ExplosionType.Fireball);
			//col.collider.GetComponent<CircleCollider2D> ().radius = col.collider.GetComponent<CircleCollider2D> ().radius * 3;
			fireball.transform.SetParent( col.transform);
			fireball.transform.position = col.transform.position;
			fireball.SetActive (true);
		}



	}

}

