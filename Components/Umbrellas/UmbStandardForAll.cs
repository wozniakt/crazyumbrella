﻿using UnityEngine;
using System.Collections;

public class UmbStandardForAll : MonoBehaviour
{


	public delegate void Reflect();
	public event Reflect onReflect;


//	public void triggerReflect(){
//		if (onReflect!=null) {
//			onReflect ();
//		}
//	}
	void OnEnable ()
	{
		HUDManager.instance.bonusButtonUmbrella.interactable = false;

		this.onReflect += BonusManager.instance.GiveBonusToUmbrella;
	}
	void OnDisable(){

		this.onReflect -= BonusManager.instance.GiveBonusToUmbrella;
	}

	void OnCollisionEnter2D (Collision2D col){
		if (onReflect!=null) {
						onReflect ();
					}
		col.gameObject.layer = 8;
		BonusManager.instance.reflectCounter += 1;

//		Vector2 moveDirection = col.collider.attachedRigidbody.velocity;
//		if (moveDirection != Vector2.zero) {
//			float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
//			col.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
//		}
		
		}
	}
