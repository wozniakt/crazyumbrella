﻿using UnityEngine;
using System.Collections;

public class SwitchWeaponShield : MonoBehaviour
{

	Sprite spriteUmbShield;
	Sprite spriteUmbShoot;
	Umbrella oUmbrella;
	Collider2D col;
	void Start ()
	{
		oUmbrella = GetComponent<UmbrellaMonoModel> ().oUmbrella;
		//spriteUmbShield=new Sprite();
		//spriteUmbShoot = new Sprite ();
		col=GetComponent<Collider2D>();
		spriteUmbShield =  (Resources.Load<Sprite>("Sprites/Other/UmbShield"));
		spriteUmbShoot = (Resources.Load<Sprite>("Sprites/Other/UmbShoot"));

	}


	void OnEnable(){
		GlobalEventsManager.instance.onSwitchMode+=switchMode;

	}
	void OnDisable(){

		GlobalEventsManager.instance.onSwitchMode -= switchMode;
	}


	void switchMode(){
		//Debug.Log ("SWITCH MODE!!!");
		if (DataManager.instance.currentUmbMode==UmbrellaMode.shield) {

			DataManager.instance.currentUmbMode = UmbrellaMode.shooting;
			//Debug.Log ("SWITCH MODE!!! to shooting" +DataManager.instance.currentUmbMode);
			//this.GetComponent<SpriteRenderer> ().color = Color.black;
			this.GetComponent<SpriteRenderer> ().sprite = spriteUmbShoot;
			col.enabled = false;

		}

		else if (DataManager.instance.currentUmbMode==UmbrellaMode.shooting) {

			DataManager.instance.currentUmbMode = UmbrellaMode.shield;
			//Debug.Log ("SWITCH MODE!!! to shield"+DataManager.instance.currentUmbMode);
			//this.GetComponent<SpriteRenderer> ().color = Color.yellow;
			this.GetComponent<SpriteRenderer> ().sprite = spriteUmbShield;
			col.enabled = true;
		}


	}
}

