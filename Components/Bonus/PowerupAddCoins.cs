﻿using UnityEngine;
using System.Collections;

public class PowerupAddCoins : MonoBehaviour
{

	// Use this for initialization
	public int CoinsAmount;
	public GameObject particlesGO;
	Rigidbody2D rigidBody2d;
	void Start(){

		rigidBody2d = GetComponent<Rigidbody2D> ();
		//GlobalEventsManager.instance.onPointsChange -= this.AddCoins;
	}

	void OnDisable(){
		particlesGO.SetActive (false);
	}

	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other){ 
		if (other.CompareTag ("Hero")) {
			GlobalEventsManager.instance.triggerGetPoints (CoinsAmount,0);
			gameObject.SetActive (false);
		}
		if (other.CompareTag ("Umbrella")) {
			GlobalEventsManager.instance.triggerGetPoints (CoinsAmount,0);
			gameObject.SetActive (false);
		}
		if (other.CompareTag ("Ground")) {
			particlesGO.SetActive (true);
			rigidBody2d.velocity = new Vector2 (0, 0);

		}

	}
//	void Update(){
//
//		Debug.Log (this.name+" "+ this.rigidBody2d.velocity);
//
//	}
}

