﻿using UnityEngine;
using System.Collections;

public class Flickering : MonoBehaviour
{
	Renderer renderer;
	public float DelayTime, IntervalTime;
	// Use this for initialization
	void OnEnable ()
	{
		//renderer =new Renderer();
		renderer = GetComponent<Renderer> ();
		StartCoroutine (flickering (DelayTime, IntervalTime));
	}
	
	// Update is called once per frame
	IEnumerator  flickering (float DelayTime, float IntervalTime)
	{
		yield return new WaitForSeconds(DelayTime);
		for (float f = 1f; f >= 0; f -= 0.1f) {
			
			Color c = renderer.material.color;
			c.a = f;
			renderer.material.color = c;
			yield return null;
		}
		//Debug.Log ("Fade out");
		yield return new WaitForSeconds(IntervalTime);
		for (float f = 0f; f <= 1; f += 0.1f) {

			Color c = renderer.material.color;
			c.a = f;
			renderer.material.color = c;
			yield return null;
		}
		//Debug.Log ("Fade in");
		yield return new WaitForSeconds(IntervalTime);
		StartCoroutine (flickering (0, IntervalTime));

	}
	void OnDisable(){
		Color c = renderer.material.color;
		c.a = 1;
		renderer.material.color = c;
	}
}

