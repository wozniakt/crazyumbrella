﻿using UnityEngine;
using System.Collections;

public class PowerupAddHealth : MonoBehaviour
{

	// Use this for initialization
	public int HealthAmount;
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other){ 
		if (other.CompareTag ("Hero")) {
			
			PlayerSingleton.instance.GetComponent<CreatureMonoModel> ().oCreature.Health += HealthAmount;
			gameObject.SetActive (false);
		}
		if (other.CompareTag ("Umbrella")) {
			
			PlayerSingleton.instance.GetComponent<CreatureMonoModel> ().oCreature.Health += HealthAmount;
			gameObject.SetActive (false);
		}
	
	}
}

