﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class alphaTestPixel2 : MonoBehaviour{

	List<Vector2> vecsList;
	float vecX, vecY;
	int magicCounter, XpieceWidth,YpieceHeight ;
	float blockWidth, blockHeight;
	float divider;
	Texture2D texture;
	public static alphaTestPixel2 instance;


	private void Awake()
	{
		if (instance == null) {
			//	DontDestroyOnLoad (transform.root.gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
			return;
		}
	}

	void Start(){
		XpieceWidth = 50;
		YpieceHeight = 50;

		Renderer rend = GetComponent<Renderer>();

		// duplicate the original texture and assign to the material
		texture = Instantiate(rend.material.mainTexture) as Texture2D;
		rend.material.mainTexture = texture;
		magicCounter = 0;
		float dividerX = texture.width/XpieceWidth;
		float dividerY = texture.height/YpieceHeight;
		vecsList=new List<Vector2>();
		int i=0;
		int j = 0;
		//Debug.Log (dividerX);
		do {i=0;
			j=j+1;
			vecY=(j*(texture.height / dividerY));
			do {
				i=i+1;
				vecX = (i*(texture.width / dividerX));
				vecsList.Add (new Vector2(vecX, vecY));

			} while (i<dividerX-1);

		} while (j<dividerY-1);

		foreach (var oVec in vecsList) {
		//	Debug.Log	(oVec.x + " / " + oVec.y);
		}

//		for (int i = 1; i <4; i++) {
//			vecX = texture.width / i;
//			vecsList.Add (new Vector2(vecX, vecY));
//			Debug.Log (vecsList[i-1]);
//		}
//


}

	public void rectAlphaChange(){
		magicCounter =Random.Range( 0,vecsList.Count -1);
	//	Debug.Log (magicCounter +"magicCounter");
		blockWidth = texture.width / XpieceWidth;
		blockHeight = texture.height / YpieceHeight;
		pixelDis (vecsList [magicCounter].x, vecsList [magicCounter].y, XpieceWidth, YpieceHeight);
		vecsList.RemoveAt (magicCounter);
	
	}


	void pixelDis(float x,float y, float blockWidth, float blockHeight){



		// colors used to tint the first 3 mip levels
		Color[] colors = new Color[1];
		colors[0] = new Color (1,1,1,0);
		//		colors[1] = new Color (1,1,1,0);
		//		colors[2] = new Color (1,1,1,0);
		int mipCount = Mathf.Min(1, texture.mipmapCount);

		// tint each mip level
		for( var mip = 0; mip < mipCount; ++mip ) {
			var cols = texture.GetPixels( mip );
			for( var i = 0; i < cols.Length; ++i ) {
				cols[i] = Color.Lerp(cols[i], colors[mip], 1f);
			}
			texture.SetPixels((int)x,(int)y, (int)blockWidth,(int)blockHeight,cols );
		}
		// actually apply all SetPixels, don't recalculate mip levels
		texture.Apply(false);

	}

}