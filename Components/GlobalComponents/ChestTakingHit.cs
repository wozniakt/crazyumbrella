﻿using UnityEngine;
using System.Collections;

public class ChestTakingHit : MonoBehaviour
{
	Animator animator;
	GameObject bulletExplosion;
	// Use this for initialization
	void Start ()
	{
		animator = GetComponent<Animator>();
		animator.ResetTrigger ("HasBeenHit");
	}
	
	void OnCollisionEnter2D (Collision2D col){
		if( col.rigidbody.CompareTag("Bullet")) {

			animator.SetTrigger ("HasBeenHit");
			bulletExplosion=PoolManager.instance.GetPooledObject_Explosion(col.transform.GetComponent<BulletMonoModel>().oBullet.Explosion);
			col.transform.gameObject.SetActive (false);
			bulletExplosion.transform.position = col.transform.position;
			bulletExplosion.SetActive (true);
	
			//oCreature.TakeDamage (other.GetComponent<BulletMonoModel>().oBullet.Power);
			GlobalEventsManager.instance.triggerGetPoints (-1, 0);


		}
}
}
