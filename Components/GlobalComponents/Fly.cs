﻿using UnityEngine;
using System.Collections;

public class Fly : MonoBehaviour
{
	public float horSpeed, verSpeed, timeDelay;
	Rigidbody2D thisRigidbody2d;
	// Use this for initialization
	void OnEnable ()
	{
		thisRigidbody2d = this.GetComponent<Rigidbody2D> ();
		StartCoroutine(startFly(Random.Range(0,timeDelay)));
	}



	// Update is called once per frame
	IEnumerator startFly (float delayTime)
	{
		yield return new WaitForSeconds (delayTime);
		thisRigidbody2d.velocity = new Vector2 (horSpeed,Random.Range(4,verSpeed));
		transform.right = -thisRigidbody2d.velocity.normalized;
	}
}

