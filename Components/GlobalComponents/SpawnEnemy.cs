﻿using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour
{
	GameObject currentEnemy;
	public float spawningDirectionX, spawningDirectionY;
	public float spawnSingleEnemyInterval;
	public float spawningSeriesAmount;
	public float spawningSeriesInterval,rangeSpawn;
	//float EnemySpawnTimer;
	//float enemyTypeCounter;
	void Start ()
	{
		//enemyTypeCounter = 0;
		//EnemySpawnTimer =0;
		if (DataManager.instance.maxEnemiesCount<=20) {
			DataManager.instance.maxEnemiesCount=DataManager.instance.currentStage.Number;
		} else {
			DataManager.instance.maxEnemiesCount=20;
		}//vers with points and stage level
		StartCoroutine (spawningEnemies ());
		//StartCoroutine (spawningEnemies());
	}

	void Update(){
		//EnemySpawnTimer += Time.deltaTime;
		//DataManager.instance.maxEnemiesCount=(int)Mathf.Round((EnemySpawnTimer/20)+01f); THIS IS VERSION WITH TIMER 
//		if (DataManager.instance.maxEnemiesCount<=20) {
//			DataManager.instance.maxEnemiesCount=DataManager.instance.currentStage.Number;
//		} else {
//			DataManager.instance.maxEnemiesCount=20;
//		}//vers with points and stage level

	}


	IEnumerator spawningEnemies(){
		yield return new WaitForSeconds (3);
		//yield return new WaitForSeconds (spawnSingleEnemyInterval);
		if (DataManager.instance.currentEnemiesCount < DataManager.instance.maxEnemiesCount && DataManager.instance.oGameState==GameState.GameIsOn) {
			for (int i = DataManager.instance.currentEnemiesCount; i < DataManager.instance.maxEnemiesCount; i++) {

				singleEnemySpawn ();
				yield return new WaitForSeconds (spawnSingleEnemyInterval+Random.Range(0,5));
			}

	}
		StartCoroutine (spawningEnemies ());
	}

	void singleEnemySpawn(){
		int randEnemDownbound = DataManager.instance.currentStage.Number - 2;
		int randEnemUpbound = DataManager.instance.currentStage.Number + 1;
		int enemyNumber = (int)Random.Range (randEnemDownbound, randEnemUpbound);
		//Debug.Log (randEnemDownbound +"   " + randEnemUpbound+"random enemy:"+ enemyNumber);
		//EnemyType randEnemyType = enType;

		currentEnemy=PoolManager.instance.GetPooledObject_Enemy(enemyNumber);

		//Debug.Log (enemyUfo.name);
		currentEnemy.transform.position = new Vector2 ((this.transform.position.x - (3 * spawningDirectionX)), this.transform.position.y-Random.Range(0,rangeSpawn));
		currentEnemy.SetActive (true);
		currentEnemy.GetComponent<CreatureMonoModel> ().oCreature.HorizontalSpeed = -currentEnemy.GetComponent<CreatureMonoModel> ().oCreature.HorizontalSpeed  * spawningDirectionX*Random.Range(1,2);
	}

	//UNSPAWN Enemy
	void OnTriggerEnter2D(Collider2D other){

		if (other.CompareTag ("Enemy")) {
			other.gameObject.SetActive (false);
		}
	}
}

