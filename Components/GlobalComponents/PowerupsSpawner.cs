﻿using UnityEngine;
using System.Collections;

public class PowerupsSpawner : MonoBehaviour
{
	GameObject powerup;
	public float spawningDirectionX, spawningDirectionY;
	public float spawnSinglePowerupInterval;
	public float spawningSeriesAmount;
	public float rangeSpawn;

	void Start ()
	{
		StartCoroutine (spawningPowerups ());
	}

	IEnumerator spawningPowerups(){
		yield return new WaitForSeconds (spawnSinglePowerupInterval);
				singlePowerupSpawn ();
		StartCoroutine (spawningPowerups ());
	}

	void singlePowerupSpawn(){
		PowerupType randPowerupType = (PowerupType)Random.Range (1, 3);
		powerup=PoolManager.instance.GetPooledObject_Powerup(randPowerupType);
		powerup.transform.position = new Vector2 ((this.transform.position.x - (spawningDirectionX)-Random.Range(0,rangeSpawn)), this.transform.position.y);
		powerup.SetActive (true);
	}

	void OnTriggerEnter2D(Collider2D other){

		if (other.CompareTag ("Powerup")) {
			other.gameObject.SetActive (false);
		}
	}
}

