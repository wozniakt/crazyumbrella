﻿using UnityEngine;
using System.Collections;

public class AIcontrollerTakingDamage : MonoBehaviour
{
	TakingDamage takingDamage;
	Creature oCreature;

	void Start ()
	{
		takingDamage = GetComponent<TakingDamage> ();
		oCreature = GetComponent<CreatureMonoModel> ().oCreature;
	}


	void OnTriggerEnter2D(Collider2D other){ 
		takingDamage.TakeDamage (other);
		CreatureHpLoss();
	}

	void CreatureHpLoss()
	{	if (oCreature.Health<=0) {
				GlobalEventsManager.instance.triggerGetPoints(oCreature.MaxHealth,1);
				GameObject enemyExplosion=PoolManager.instance.GetPooledObject_Explosion(oCreature.ExplosionType);
				enemyExplosion.transform.position = this.transform.position;
				enemyExplosion.SetActive (true);
				this.gameObject.SetActive(false);
				GameObject rainDrop=PoolManager.instance.GetPooledObject_Bullet(BulletType.RainDrop);
				rainDrop.transform.position = this.transform.position;
				rainDrop.SetActive (true);
		}
	}
}

