﻿using UnityEngine;
using System.Collections;

public  class TakingDamage : MonoBehaviour
{
	Creature oCreature;
	GameObject bulletExplosion;
	void Start ()
	{
		oCreature = GetComponent<CreatureMonoModel> ().oCreature;
	}

	//TODO
//	void OnTriggerEnter2D(Collider2D other){ // tu tego ma nie byc -- ma to sie pojawic, dokaldniet  to, w AiTakeDamageCtrl oraz w PlayerTakeDamageCtrl. Z tym, ze w Ai musisz dac jescze trigger eventa z globalevents 'GlobalEventsManager.instance.triggerGetPoints(oCreature.MaxHealth);'


	public  void TakeDamage(Collider2D other){
		if( other.CompareTag("Bullet")) {

			bulletExplosion=PoolManager.instance.GetPooledObject_Explosion(other.GetComponent<BulletMonoModel>().oBullet.Explosion);
			other.gameObject.SetActive (false);
			bulletExplosion.transform.position = other.transform.position;
			bulletExplosion.SetActive (true);
			oCreature.TakeDamage (other.GetComponent<BulletMonoModel>().oBullet.Power);

		}
	}


}

