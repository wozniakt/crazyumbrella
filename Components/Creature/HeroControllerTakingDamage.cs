﻿using UnityEngine;
using System.Collections;

public class HeroControllerTakingDamage : MonoBehaviour
{
	TakingDamage takingDamage;
	Creature oCreature;

	void Start ()
	{
		takingDamage = GetComponent<TakingDamage> ();
		oCreature = GetComponent<CreatureMonoModel> ().oCreature;
		//oCreature.Health = oCreature.MaxHealth ;
	}


	void OnTriggerEnter2D(Collider2D other){ 
		takingDamage.TakeDamage (other);
		CreatureHpLoss();
	}

	void CreatureHpLoss()
	{	
		if (oCreature.Health<=0) {
			GlobalEventsManager.instance.triggerGameStateChange(GameState.StageLost);
			GameObject heroExplosion=PoolManager.instance.GetPooledObject_Explosion(oCreature.ExplosionType);
			heroExplosion.transform.position = this.transform.position;
			heroExplosion.SetActive (true);
			this.gameObject.SetActive(false);
			oCreature.TriggerPlayerDeath (GameState.StageLost);
		}
	}
}

