﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
	Creature oCreature;
	Rigidbody2D thisRigidbody2d;

	// Use this for initialization
	void OnEnable ()
	{	
		thisRigidbody2d = this.GetComponent<Rigidbody2D> ();
		oCreature = GetComponent<CreatureMonoModel> ().oCreature;
		this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (oCreature.HorizontalSpeed,  oCreature.VerticalSpeed);

	}
	
	// Update is called once per frame
	void Update ()
	{
		this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (oCreature.HorizontalSpeed, oCreature.VerticalSpeed);
		transform.right = -thisRigidbody2d.velocity.normalized;
	}
}

