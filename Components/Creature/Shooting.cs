﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour
{
	[HideInInspector]
	public Vector2 targetPoint;
	GameObject Target;
	GameObject ufoBullet;
	Rigidbody2D bulletRigidBody2D;
	[HideInInspector]
	public float inaccuracy;
	[HideInInspector]
	public BulletType bulletType;
	[HideInInspector]
	public float BulletSpeed;
	[HideInInspector]
	public float BulletsAmount;
	[HideInInspector]
	public float BulletInterval;
	[HideInInspector]
	public float shootingSeriesAmount;
	[HideInInspector]
	public float seriesInterval;
	// Use this for initialization

	public void FireAtWill()
	{
		StartCoroutine(ShootingAtTarget(seriesInterval));

	}

		 IEnumerator ShootingAtTarget(float seriesInterval){

		for (int i = 0; i < shootingSeriesAmount; i++) {
			for (int j = 0; j < BulletsAmount; j++) {
				yield return new WaitForSeconds (BulletInterval);
				singleSerieShoots();
			}
			yield return new WaitForSeconds (seriesInterval+Random.Range(0,3));
		}
	}


	public void singleSerieShoots(){
		
		ufoBullet=PoolManager.instance.GetPooledObject_Bullet (bulletType);
		ufoBullet.transform.position = new Vector2 (this.transform.position.x, this.transform.position.y -0.2f);
		if (this.CompareTag("Enemy")) {
			ufoBullet.layer= 10;
		}
		ufoBullet.SetActive (true);
		bulletRigidBody2D = ufoBullet.GetComponent<Rigidbody2D> ();
		Vector2 direction = (new Vector2(targetPoint.x, targetPoint.y-Random.Range(-inaccuracy,inaccuracy)))-(Vector2)ufoBullet.transform.position;
		direction.Normalize();
		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
		ufoBullet.transform.rotation = Quaternion.Euler(0f, 0f, angle - 45);
		bulletRigidBody2D.velocity = direction * BulletSpeed;
	}

	//************************************* new shooting 
	public IEnumerator singleSerieShooting(float _bulletSpeed, float _bulletsInterval, float _bulletsAmount, BulletType _bulletType,
		float _offsetX, float _offsetY, Vector2 _targetPoint, float _inaccuracy){

		for (int i =(int) (_bulletsAmount/-2); i < (_bulletsAmount/2); i++) {
			ufoBullet = PoolManager.instance.GetPooledObject_Bullet (_bulletType);
			ufoBullet.transform.position = new Vector2 (this.transform.position.x + _offsetX, this.transform.position.y + _offsetY);
			if (this.CompareTag ("Enemy")) {
				ufoBullet.layer = 10;
			}
			if (this.CompareTag ("Hero")) {
				ufoBullet.layer = 8;
			}
			if (this.CompareTag ("Umbrella")) {
				ufoBullet.layer = 8;
			}
			ufoBullet.SetActive (true);
			bulletRigidBody2D = ufoBullet.GetComponent<Rigidbody2D> ();
			Vector2 direction = (new Vector2 (_targetPoint.x+ Random.Range (-_inaccuracy, _inaccuracy)+i, _targetPoint.y )) - (Vector2)ufoBullet.transform.position;
			direction.Normalize ();
			float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
			ufoBullet.transform.rotation = Quaternion.Euler (0f, 0f, angle - 45);
			bulletRigidBody2D.velocity = direction *_bulletSpeed;
			yield return new WaitForSeconds (_bulletsInterval);
		}

	}

	public void singleSerieShoot(float _bulletSpeed, float _bulletsInterval, float _bulletsAmount, BulletType _bulletType,
		float _offsetX, float _offsetY, Vector2 _targetPoint, float _inaccuracy){
		StartCoroutine(singleSerieShooting( _bulletSpeed,  _bulletsInterval,  _bulletsAmount,    _bulletType, _offsetX,  _offsetY,  _targetPoint,  _inaccuracy));
	
	}

}

