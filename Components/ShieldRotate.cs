﻿using UnityEngine;
using System.Collections;
using CnControls;



public class ShieldRotate : MonoBehaviour
{
	
	 GameObject hero ;
	 GameObject shieldGo ;
	 Camera mainCamera;
	 
	//public GameObject joystick;
	float horizontal, vertical;
	  
	void Start(){

		hero = GameObject.FindGameObjectWithTag ("Hero");
		shieldGo = GameObject.FindGameObjectWithTag ("Umbrella");

	}



	void Update () {

		horizontal = CnInputManager.GetAxis("Horizontal")* Time.deltaTime;
		vertical= 0.1f* Time.deltaTime;
		
		float angle = Mathf.Atan2(vertical, horizontal) * Mathf.Rad2Deg;

		shieldGo.transform.rotation = Quaternion.Euler(0, 0, angle);
		shieldGo.transform.position=new Vector3(hero.transform.position.x,hero.transform.position.y-0.1f,5); 
	}


}
