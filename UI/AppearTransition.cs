﻿using UnityEngine;
using System.Collections;

public class AppearTransition : MonoBehaviour
{ 
	 public Vector3 targetPostition;
	void OnEnable()
	{
//		targetPostition = new Vector3 (-6, -1, 0);
		//Whatever logic you have for acquiring your target
		StartCoroutine(LerpRoutine(targetPostition, 2f));
	}

	private IEnumerator LerpRoutine (Vector3 target, float duration)
	{
		Vector3 start = transform.position;
		float elapsedTime = 0.0f;
	
			while (transform.position != target) {
				elapsedTime += Time.deltaTime;
				transform.position = Vector3.Lerp (target, start, elapsedTime / duration);
				yield return null;
		}

	}
}