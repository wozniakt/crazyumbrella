﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScrollRectSnap : MonoBehaviour
{

	public RectTransform panel; // to hold scroll panel
	public Button[] bttn;
	public RectTransform center; //center to compare buttons pos
	public float[] distReposition;
	public float distToFade;
	float [] distance;
	bool dragging=false; // true=we dragg; if false, try to snap
	int bttnDistance; //distance beetween buttons
	int minButtonNum; // to hold number of the button closest to center;
	// Use this for initialization
	bool updateWorks;

	void Start()
	{
		int lastUsedHero = DataManager.instance.oPlayerData.LastUsedHero;
		//panel.GetComponent<ScrollRect> ().horizontalNormalizedPosition = 10;
		int bttnLenght=bttn.Length;
		distance=new float[bttnLenght] ;
		distReposition=new float[bttnLenght] ;
		bttnDistance =(int) Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.x-bttn[0].GetComponent<RectTransform>().anchoredPosition.x);


		LerpToBttn(-bttn[0].GetComponent<RectTransform>().anchoredPosition.x);
		updateWorks = true;
		//StartCoroutine (startScroll ());
//		for (int i = 0; i < bttn.Length; i++) {
			if (bttn [0].GetComponent<CreatureMonoModel> () != null)
			{
			//Debug.Log (lastUsedHero);
			panel.position = new Vector2 (-bttn [lastUsedHero].GetComponent<RectTransform> ().position.x, panel.position.y);
			}
			if (bttn [0].GetComponent<UmbrellaMonoModel> () != null)
			{
			panel.position = new Vector2 (-bttn [DataManager.instance.oPlayerData.LastUsedUmb].GetComponent<RectTransform> ().position.x, panel.position.y);
			}

	

	}



	// Update is called once per frame
	 void Update()
	{
		
		if (updateWorks) {
			
	
		for (int i = 0; i < bttn.Length; i++) {
			distReposition [i] = center.GetComponent<RectTransform> ().position.x - bttn [i].GetComponent<RectTransform> ().position.x;
			distance [i] = Mathf.Abs (distReposition [i]);

			if (distReposition [i] > distToFade || distReposition [i] < -distToFade) {
				Color btnColor = bttn [i].GetComponent<Image> ().color;
				bttn [i].GetComponent<Image> ().color = new Color (btnColor.r, btnColor.g, btnColor.b, 8 / (distance [i]) - 1);
			}
		
			if (distReposition [i] <= distToFade && distReposition [i] >= -distToFade) {
				Color btnColor = bttn [i].GetComponent<Image> ().color;
				bttn [i].GetComponent<Image> ().color = new Color (btnColor.r, btnColor.g, btnColor.b, 1);
			}

			if (distReposition [i] < -6 || distReposition [i] > 6) {
//				if (bttn [i].GetComponent<CreatureMonoModel> () != null) {	
//					DataManager.instance.lastUsedHero= bttn [i].GetComponent<CreatureMonoModel> ().oCreature.Id ;
//				}
				if (bttn [i].GetComponent<UmbrellaMonoModel> () != null) {	
						DataManager.instance.oPlayerData.LastUsedUmb= bttn [i].GetComponent<UmbrellaMonoModel> ().oUmbrella.Id ;
				}
			
			}




			if (distReposition [i] > 10) {
				float curX = bttn [i].GetComponent<RectTransform> ().anchoredPosition.x;
				float curY = bttn [i].GetComponent<RectTransform> ().anchoredPosition.y;

				Vector2 newAnchoredPos = new Vector2 (curX + (bttn.Length * bttnDistance), curY);
				bttn [i].GetComponent<RectTransform> ().anchoredPosition = newAnchoredPos;
//				if (bttn [i].GetComponent<CreatureMonoModel> () != null) {	
//					bttn [i].GetComponent<CreatureMonoModel> ().oCreature.LastUsed = false;
//				}
//				if (bttn [i].GetComponent<UmbrellaMonoModel> () != null) {	
//					bttn [i].GetComponent<UmbrellaMonoModel> ().oUmbrella.LastUsed = false;
//				}
			} else if (distReposition [i] < -10) {
				float curX = bttn [i].GetComponent<RectTransform> ().anchoredPosition.x;
				float curY = bttn [i].GetComponent<RectTransform> ().anchoredPosition.y;

				Vector2 newAnchoredPos = new Vector2 (curX - (bttn.Length * bttnDistance), curY);
				bttn [i].GetComponent<RectTransform> ().anchoredPosition = newAnchoredPos;

//
//				if (bttn [i].GetComponent<CreatureMonoModel> () != null) {				
//					bttn [i].GetComponent<CreatureMonoModel> ().oCreature.LastUsed = false;
//				}
//				if (bttn [i].GetComponent<UmbrellaMonoModel> () != null) {	
//					bttn [i].GetComponent<UmbrellaMonoModel> ().oUmbrella.LastUsed = false;
//				}
			}
		}

		float minDistance = Mathf.Min (distance); //get the min distance

		for (int a = 0; a < bttn.Length; a++) {
			if (minDistance == distance [a]) {
				minButtonNum = a;
			}
		}

		if (!dragging) {
			
			LerpToBttn (-bttn [minButtonNum].GetComponent<RectTransform> ().anchoredPosition.x);

			if (bttn [minButtonNum].GetComponent<CreatureMonoModel> () != null) {				
				if (bttn [minButtonNum].GetComponent<CreatureMonoModel> ().oCreature.Name != DataManager.instance.currentHero.Name) {
					GlobalEventsManager.instance.triggerGetCurrentHero (bttn [minButtonNum].GetComponent<CreatureMonoModel> ().oCreature);	
						DataManager.instance.oPlayerData.LastUsedHero= bttn [minButtonNum].GetComponent<CreatureMonoModel> ().oCreature.Id ;
				}
					
			} 

			if (bttn [minButtonNum].GetComponent<UmbrellaMonoModel> () != null) {				
				if (bttn [minButtonNum].GetComponent<UmbrellaMonoModel> ().oUmbrella.Name != DataManager.instance.currentUmbrella.Name) {
					GlobalEventsManager.instance.triggerGetCurrentUmbrella (bttn [minButtonNum].GetComponent<UmbrellaMonoModel> ().oUmbrella);
					DataManager.instance.oPlayerData.LastUsedUmb= bttn [minButtonNum].GetComponent<UmbrellaMonoModel> ().oUmbrella.Id ;
				}
			}
		}
	}
	}


	public void LerpToBttn(float position)
	{
		float newX=Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime*2f);
		Vector2 newPosition = new Vector2(newX, panel.anchoredPosition.y);
		panel.anchoredPosition=newPosition;


	}

	public void StartDrag()
	{
		dragging=true;
	}

	public void EndDrag()
	{
		dragging=false;
	}

	IEnumerator startScroll(){
		//updateWorks = false;
		yield return new WaitForSeconds (3);
		panel.position = new Vector2 (-bttn [1].GetComponent<RectTransform> ().position.x, panel.position.y);
		yield return new WaitForSeconds (3);
		panel.position = new Vector2 (-bttn [3].GetComponent<RectTransform> ().position.x, panel.position.y);
		yield return new WaitForSeconds (3);
		panel.position = new Vector2 (-bttn [2].GetComponent<RectTransform> ().position.x, panel.position.y);
		//updateWorks = true;
		//StartDrag ();
		//		LerpToBttn(-bttn[5].GetComponent<RectTransform>().anchoredPosition.x);

		//EndDrag ();



	}


}

