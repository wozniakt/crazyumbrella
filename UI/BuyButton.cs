﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyButton : MonoBehaviour
{
	Button thisButton;
	public Text text;
	public bool isVisible;

	// Use this for initialization
	void Start ()
	{
		thisButton = this.GetComponent<Button> ();
		if (GetComponentInParent<UmbrellaMonoModel>()) {
			thisButton.onClick.AddListener(() => Buy_Umbrella());
			text.text = (GetComponentInParent<UmbrellaMonoModel> ().oUmbrella.Cost).ToString()+"$";
			isVisible = !GetComponentInParent<UmbrellaMonoModel> ().oUmbrella.IsUnlocked;

			updateThis ();

		}
		if (GetComponentInParent<CreatureMonoModel>()) {
			thisButton.onClick.AddListener(() => Buy_Creature());
			text.text = (GetComponentInParent<CreatureMonoModel> ().oCreature.Cost).ToString()+"$";
			isVisible = !GetComponentInParent<CreatureMonoModel> ().oCreature.IsUnlocked;

			updateThis ();
		}
		UIManager.instance.onUIchange += this.updateThis;
	}

	void OnDisable(){
		UIManager.instance.onUIchange -= this.updateThis;
	}
	void OnEnable(){
	//	UIManager.instance.onUIchange += this.updateThis;

	}

	// Update is called once per frame
	void Buy_Umbrella()
	{
		if (GetComponentInParent<UmbrellaMonoModel>().oUmbrella.Cost<=DataManager.instance.oPlayerData.CoinsCount) {
			GetComponentInParent<UmbrellaMonoModel>().oUmbrella.IsUnlocked = true;
			DataManager.instance.updateUmbrellaList ();
			DataManager.instance.oPlayerData.CoinsCount=DataManager.instance.oPlayerData.CoinsCount-GetComponentInParent<UmbrellaMonoModel>().oUmbrella.Cost;
			isVisible = false;
			UIManager.instance.triggerUpdateUI ();


		}
	}

	void Buy_Creature()
	{
		if (GetComponentInParent<CreatureMonoModel>().oCreature.Cost<=DataManager.instance.oPlayerData.CoinsCount) {
			GetComponentInParent<CreatureMonoModel>().oCreature.IsUnlocked = true;
			DataManager.instance.updateHeroesList ();
			DataManager.instance.oPlayerData.CoinsCount=DataManager.instance.oPlayerData.CoinsCount-GetComponentInParent<CreatureMonoModel>().oCreature.Cost;
			isVisible = false;
			UIManager.instance.triggerUpdateUI ();

		
		}
	}

	void updateThis(){
		if (GetComponentInParent<UmbrellaMonoModel> ()) {
			//Debug.Log ("UPDATED umb button" + GetComponentInParent<UmbrellaMonoModel> ().oUmbrella.Name);
			text.text = (GetComponentInParent<UmbrellaMonoModel> ().oUmbrella.Cost).ToString ()+"$";
			isVisible = !GetComponentInParent<UmbrellaMonoModel> ().oUmbrella.IsUnlocked;
			this.gameObject.SetActive (isVisible);
			//DataManager.instance.updateHeroesList ();
		}
		if (GetComponentInParent<CreatureMonoModel> ()) {
			//Debug.Log ("UPDATED hro button" + GetComponentInParent<CreatureMonoModel> ().oCreature.Name);
			text.text = (GetComponentInParent<CreatureMonoModel> ().oCreature.Cost).ToString ()+"$";
			isVisible = !GetComponentInParent<CreatureMonoModel> ().oCreature.IsUnlocked;
			this.gameObject.SetActive (isVisible);
			//DataManager.instance.updateUmbrellaList ();
		}
	}






}

