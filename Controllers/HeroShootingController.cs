﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CnControls;


public class HeroShootingController : MonoBehaviour
{
	public BulletType bulletType;
	public float BulletSpeed;
	public float BulletsAmount;
	public float BulletInterval;
	public float seriesInterval;
	public float seriesAmount;
	Vector2 targetPoint;
	public float inaccuracy, offsetX, offsetY;
	Shooting shootingAbility;
	GameObject Pointer;
	// Use this for initialization
	void OnEnable ()
	{	
		Pointer = GameObject.FindGameObjectWithTag ("Pointer");
		shootingAbility = GetComponent<Shooting> ();
		//GlobalEventsManager.instance.onSwitchMode += UmbShootModeSingleShoot;
		StartCoroutine (UmbShootModeSingleShoot ());
	}

	void OnDisable ()
	{	
		
		//GlobalEventsManager.instance.onSwitchMode -= UmbShootModeSingleShoot;
	}

	 IEnumerator HeroShoot(){

		for (int i = 0; i < seriesAmount; i++) {
			targetPoint =new Vector2(Pointer.transform.position.x,Pointer.transform.position.y);
			shootingAbility.singleSerieShoot (8 , 0, 10, bulletType, offsetX, offsetY, targetPoint, inaccuracy);
			yield return new WaitForSeconds (seriesInterval);
		}

	}
	public void HeroShooting(){
		BonusManager.instance.reflectCounter = 0;
		HUDManager.instance.bonusButtonUmbrella.interactable = false;
		StartCoroutine (HeroShoot ());
	}

	public void UmbShootingModeShooting(){
		//if (DataManager.instance.currentUmbMode==UmbrellaMode.shooting) {
			//StartCoroutine (UmbShootModeSingleShoot ());
		//}

	}

	IEnumerator UmbShootModeSingleShoot(){
		//Debug.Log ("satrts shooting");
		if (DataManager.instance.currentUmbMode==UmbrellaMode.shooting) {
		//for (int i = 0; i < seriesAmount; i++) {
			targetPoint =new Vector2(Pointer.transform.position.x,Pointer.transform.position.y);
			shootingAbility.singleSerieShoot (BulletSpeed , BulletInterval, BulletsAmount, bulletType, offsetX, offsetY, targetPoint, inaccuracy);
			yield return new WaitForSeconds (2);
		//}
		}
		yield return new WaitForSeconds (1);
		StartCoroutine (UmbShootModeSingleShoot ());
	}
//	void Update(){
//		if (DataManager.instance.currentUmbMode == UmbrellaMode.shooting) {
//			targetPoint =new Vector2(Pointer.transform.position.x,Pointer.transform.position.y);
//			shootingAbility.singleSerieShoot (BulletSpeed , BulletInterval, BulletsAmount, bulletType, offsetX, offsetY, targetPoint, inaccuracy);
//
//		}
//	}



}

