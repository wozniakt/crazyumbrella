﻿using UnityEngine;
using System.Collections;
using CnControls;

public class HeroMoveController : MonoBehaviour
{
	float horizontal;
	Rigidbody2D rigidBody2d;
	Creature oCreature;

	// Use this for initialization
	void Start ()
	{
		oCreature =PlayerSingleton.instance.GetComponent<CreatureMonoModel> ().oCreature;

		rigidBody2d = this.GetComponent<Rigidbody2D> ();

	}
	
	// Update is called once per frame
	void Update ()
	{
	//	Debug.Log (horizontal);
		horizontal = CnInputManager.GetAxis("HorizontalMove");
		if (horizontal > 0) {
			rigidBody2d.velocity = new Vector2 (oCreature.HorizontalSpeed, 0);
			transform.forward= (new Vector3(0,0, 1));
		}
		if (horizontal < 0) {
			rigidBody2d.velocity = new Vector2 (-oCreature.HorizontalSpeed, 0);
			transform.forward= (new Vector3(0,0, -1));
		} if (horizontal == 0) {
			
			rigidBody2d.velocity = new Vector2 (0, 0);

		}
		//Debug.Log (horizontal+" "+rigidBody2d.velocity);
	}
}

