﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI_MovementShootingController : MonoBehaviour
{
	public BulletType bulletType;
	public float BulletSpeed;
	public float BulletsAmount;
	public float BulletInterval;
	public float shootingSeriesAmount;
	public float seriesInterval;
	Shooting shootingAbility;
	Vector2 targetPoint;
	Creature oCreature;
	public List<Vector3> movesList = new List<Vector3>();  
	public float inaccuracy;
	Transform hero,chest;
	void OnEnable ()
	{	
		shootingAbility = GetComponent<Shooting> ();
		oCreature = GetComponent<CreatureMonoModel> ().oCreature;

		StartCoroutine(movementV1());
		if (GameObject.FindWithTag ("Hero")!=null) {
			hero = GameObject.FindWithTag ("Hero").transform;
			chest = GameObject.FindWithTag ("Chest").transform;
			StartCoroutine(shootingV1());
		} 
	}

	IEnumerator shootingV1(){
		//shootingAbility.targetPoint =new Vector2( targetPoint.x,targetPoint.y);
		shootingAbility.BulletInterval = BulletInterval;
		shootingAbility.BulletsAmount = BulletsAmount;
		shootingAbility.BulletSpeed = BulletSpeed;
		shootingAbility.seriesInterval = seriesInterval;
		shootingAbility.bulletType = bulletType;
		shootingAbility.shootingSeriesAmount = shootingSeriesAmount;
		if ((Random.Range(0,2)==0) ){
			shootingAbility.targetPoint = hero.position;
		}
			else {
				shootingAbility.targetPoint = chest.position;
			}

		shootingAbility.FireAtWill ();

		yield return new WaitForSeconds (seriesInterval);
		StartCoroutine(shootingV1());

	}

	IEnumerator movementV1(){
		foreach (Vector3 oMove in movesList) {
			oCreature.HorizontalSpeed = oMove.x;
			oCreature.VerticalSpeed = oMove.y;
			yield return new WaitForSeconds (oMove.z);
		}
		StartCoroutine (movementV1());
	}


	// Update is called once per frame
	void Update ()
	{
		
	}
}

