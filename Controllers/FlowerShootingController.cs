﻿using UnityEngine;
using System.Collections;

public class FlowerShootingController : MonoBehaviour
{
	public BulletType bulletType;
	public float BulletSpeed;
	public float BulletsAmount;
	public float BulletInterval;
	public float seriesInterval;
	public float seriesAmount;
	Vector2 targetPoint;
	public float inaccuracy, offsetX, offsetY;
	Shooting shootingAbility;

	// Use this for initialization
	void OnEnable ()
	{	
		
		shootingAbility = GetComponent<Shooting> ();

	}
	// Use this for initialization
	IEnumerator FlowerShoot(){

		for (int i = 0; i < seriesAmount; i++) {
			targetPoint =new Vector2(this.transform.position.x,this.transform.position.y+4);
			shootingAbility.singleSerieShoot (BulletSpeed , BulletInterval, BulletsAmount, bulletType, offsetX, offsetY, targetPoint, inaccuracy);
			yield return new WaitForSeconds (seriesInterval);
		}
		this.gameObject.SetActive (false);

	}
	public void FlowerShooting(){
		StartCoroutine (FlowerShoot ());
	}
}

