﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI_SideKickShootingController : MonoBehaviour
{
	public BulletType bulletType;
	public float BulletSpeed;
	public float BulletsAmount;
	public float BulletInterval;
	public float shootingSeriesAmount;
	public float seriesInterval;
	public Vector2 targetPoint;
	public float inaccuracy;
	Shooting shootingAbility;

	// Use this for initialization
	void OnEnable ()
	{	
		shootingAbility = GetComponent<Shooting> ();
			shootingV1 ();

	}

	void shootingV1(){
		shootingAbility.targetPoint = targetPoint;
		shootingAbility.BulletInterval = BulletInterval;
		shootingAbility.BulletsAmount = BulletsAmount;
		shootingAbility.BulletSpeed = BulletSpeed;
		shootingAbility.seriesInterval = seriesInterval;
		shootingAbility.bulletType = bulletType;
		shootingAbility.shootingSeriesAmount = shootingSeriesAmount;
		shootingAbility.inaccuracy = inaccuracy;
		shootingAbility.FireAtWill ();

	}

}

