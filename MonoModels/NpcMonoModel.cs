﻿using UnityEngine;
using System.Collections;

public class NpcMonoModel : MonoBehaviour
{
	public NpcPlaceHolder oNpcPlaceHolder;
	public GameObject appearExplosion;

	void Start ()
	{
		oNpcPlaceHolder = GetComponent<NpcMonoModel> ().oNpcPlaceHolder;
		GlobalEventsManager.instance.OnNpcActivate += activateNPC;
	}
	void OnDisable(){
		GlobalEventsManager.instance.OnNpcActivate -= activateNPC;
	}


	void activateNPC(int highscore){

		if (highscore/50>=this.oNpcPlaceHolder.Id && this.oNpcPlaceHolder.IsUnlocked==false) {
			
			GameObject NPC=(GameObject)Instantiate(Resources.Load("Prefabs/NPCs/NPC" +this.oNpcPlaceHolder.Id));
			NPC.transform.localPosition = this.transform.localPosition;
			//appearExplosion.transform.localPosition = this.transform.localPosition;
			appearExplosion.SetActive (true);
			this.oNpcPlaceHolder.IsUnlocked = true;
		}
	}
}

