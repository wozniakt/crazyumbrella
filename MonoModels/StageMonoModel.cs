﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StageMonoModel : MonoBehaviour
{
	public Stage oStage;

	// Use this for initialization
	void Start ()
	{
		this.GetComponent<Button>().onClick.AddListener(() => DataManager.instance.StartGameFromMenuOrRestart(this.GetComponent<StageMonoModel>().oStage));
			GetComponentInChildren<Text> ().text = (this.oStage.Number).ToString();
		if (this.oStage.Number<=DataManager.instance.oPlayerData.MaxUnlockedStage) {
			this.oStage.IsUnlocked = true;
		}

		if (!this.oStage.IsUnlocked) {
			Image img = GetComponent<Image> ();
			img.color = new Color (0, 0, 0);
		}
	}
	

}

