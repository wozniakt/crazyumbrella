﻿using UnityEngine;
using System.Collections;

public class RainDrop : MonoBehaviour
{

	// Use this for initialization

	Rigidbody2D thisRigidbody2d;


	void OnEnable ()
	{	
		thisRigidbody2d = this.GetComponent<Rigidbody2D> ();

		this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0,  -1);
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag("Hero")) {
			this.gameObject.SetActive (false);
		}

		if (other.CompareTag("Umbrella")) {
			this.gameObject.SetActive (false);
		}
		if (other.CompareTag("Ground")) {

			GameObject explosion= PoolManager.instance.GetPooledObject_Explosion(ExplosionType.Splash);
			explosion.transform.position = this.transform.position;
			explosion.SetActive (true);
			this.gameObject.SetActive (false);
			//Debug.Log ("Flowe" + other.name);

			GameObject flower= PoolManager.instance.GetPooledObject_Flower("Flower"+other.name);
			flower.transform.position = this.transform.position;
			flower.SetActive (true);
			this.gameObject.SetActive (false);
			Debug.Log (flower.transform.position + "   " + this.transform.position);
		}
		if (other.CompareTag("Flower")) {
			GameObject explosion= PoolManager.instance.GetPooledObject_Explosion(ExplosionType.Splash);
			explosion.transform.position = this.transform.position;
			explosion.SetActive (true);
			this.gameObject.SetActive (false);
			other.GetComponent<FlowerShootingController> ().FlowerShooting ();
		}

	}


}

