﻿using UnityEngine;
using System.Collections;

public class CreatureMonoModel : MonoBehaviour
{

		public Creature oCreature;
		GameObject enemyExplosion;

		void Start(){
		
	}

		void OnEnable(){
		oCreature = GetComponent<CreatureMonoModel> ().oCreature;

		oCreature.HorizontalSpeed = oCreature.MaxSpeed;
		oCreature.Health = oCreature.MaxHealth;

		//to poniżej można przerzucić do Creature class 
		if (CompareTag("Enemy")) {
			DataManager.instance.currentEnemiesCount += 1;
		}


		}

		void OnDisable(){
		if (CompareTag("Enemy")) {
			DataManager.instance.currentEnemiesCount -= 1;
		}
		}



	}
